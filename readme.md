Used only when creating new server in https://gitlab.com/bettersolutions/infrastructure/ansible/playbook-runners/securing-root-user.

User management after first creation is in https://gitlab.com/bettersolutions/infrastructure/ansible/playbook-runners/user-management
